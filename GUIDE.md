     ### Mini Trello CLI

#### Tasks

| ID   | Name            | Description                                    |
| ---- | --------------- | ---------------------------------------------- |
| 01   | Create Tasks    | As user I can create a Task with a description |
| 02   | Update Tasks    | As user I can update a Task                    |
| 03   | Delete Tasks    | As user I can delete a Task                    |
| 04   | Mark as Done    | As user I can Mark a Task as completed         |
| 05   | List Tasks      | As user I can get a list of all tasks          |
| 06   | Search Tasks    | As user I can search a task                    |
| 07   | Duplicate Tasks | As user I can get Duplicate Tasks              |
| 08   | Export Task     | As user I can export Task data                 |

#### Lists

| ID   | Name          | Desciption                          |
| ---- | ------------- | ----------------------------------- |
| 01   | Create a List | As user I can create a List         |
| 02   | Add Task      | As user I can add tasks to List     |
| 03   | Delete List   | As user I can delete List           |
| 04   | Export List   | As user I can Export List in a file |
| 05   | Import        | As user I can Import List from file |
| 06   | Search List   | As user I can search a List         |

### Boards

| ID   | Name          | Description                                         |
| ---- | ------------- | --------------------------------------------------- |
| 01   | Create Board  | As user I can create a Board container of Task List |
| 02   | Add List      | As user I can add new list of tasks into the board  |
| 03   | Delete Board  | As user I can delete Boards                         |
| 05   | Search Boards | As user I can Search a Specific Board               |

#### Create project

```bash
mkdir smtodo
```

Create gem

```bash
bundle gem smtodo
```
Build gem
```bash
gem build smtodo.gemspec
```

Install gem
```bash
gem install smtodo-0.1.0.gem
```