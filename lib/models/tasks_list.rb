require_relative '../modules/get_data.rb'
require_relative '../modules/store_data.rb'
require_relative './task.rb'

# Model that represent a List of Tasks
class TaskList

    prepend StoreData # Import the module StoreData to export and import files with tasks

    @@task_list_number = 0 # Store the number of existing task lists in the application.
    attr_accessor :task_list, :name, :number
    
    # Constructor of class TaskList
    # name: a basic task list identifier
    def initialize(name="Default")
        self.name = name
        self.task_list = []    
        self.number = @@task_list_number # Work as a task list id
        @@task_list_number += 1
    end

    # Method that store a new task into the list and call a get_data module to get task info
    def add()
        data = get_data([:name,:description])
        self.task_list << Task.new(data)
    end

    # Method that update a specified task and call a get_data module to get task info
    # id: identifier of task that will be updated
    def update_task(id)
        element = self.task_list.select {|item| item.number == id}
        data = get_data([:name,:description,:done])
        data[:done] = data[:done] == "x"? true : false 
        element[0].update(data)
    end

    # Method that delete a specified task
    # id: identifier of task that will be deleted
    def delete_item(id)
        self.task_list.reject! {|item| item.number == id}
    end

    # Method that uses a regex to filter the task list
    # name: string to be search on the name of each task
    def search_item(name)
        element = self.task_list.select {|item| item.name.scan(/#{name}/).length > 0}
        element.each_with_index {|item,index| puts item}
    end

    # Method that show each task on screen
    def show_list
        self.task_list.each_with_index {|item,index| puts item}
    end

    # Method that call the module method to store the tasks info into a file
    def export(ruta)
        self.write(self.task_list,ruta)
    end

    # Method that call the module method to get info from a file 
    # and create the tasks from the returned object
    def import(ruta)
        data = self.read(ruta)
        data.each do |item|
            p item
            task = {
                :name => item[1],
                :description => item[2],
                :done => item[4]
            }
            self.task_list << Task.new(task)
        end
    end

    # Show the Task menu
    def check_action(val)
        resp = true
        case val
        when 1
            self.add
        when 2
            print "\nElement to update : "
            id = gets.chomp.to_i
            self.update_task(id)
        when 3
            print "\nElement to delete : "
            id = gets.chomp.to_i
            self.delete_item(id)
        when 4
            print "\nName to search : "
            name = gets.chomp
            self.search_item(name)
        when 5
            puts "\nShow Tasks"
            self.show_list
        when 6
            puts "\nExport"
            self.export("export.txt")
        when 7
            puts "\nImport"
            self.import("export.txt")
        when 8
            puts "\nBack"
            resp = false
        end
        return resp
    end

    private
        # Call the get_data_task method from a module GetData
        # params: a list of keys to obtain user info
        def get_data(params)
            GetData.get_data_task(params)
        end

end