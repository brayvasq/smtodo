require_relative '../modules/get_data.rb'
require_relative './tasks_list.rb'

class Board

    attr_accessor :board_list, :name,:number
    @@board_number = 0 # Store the number of existing boards in the application.

    # Constructor of class Board
    # name: a basic board  identifier
    def initialize(name="Default")
        self.board_list = []
        self.name = name
        self.number = @@board_number # Work as a board id
        @@board_number += 1
    end

    # Method that store a new task list into the board 
    # and call a get_data module to get task list info
    def add
        data = get_data([:name])
        self.board_list << TaskList.new(data[:name])
    end

    # Method that delete a specified task list
    # id: identifier of task list to be deleted
    def delete(id)
        self.board_list.reject! {|item| item.number == id}
    end

    # Show the Task Lists menu
    def check_val(val)
        actual_list = nil
        actual_board = self
        case val
        when 1
            puts "\nCreate list"
            self.add
        when 2
            puts "\nDelete list"
            id = gets.chomp.to_i
            self.delete(id)
        when 3
            puts "\nShow List"
            self.board_list.each { |item| puts "#{item.number} | #{item.name}" }
        when 4
            puts "\nSelect List"
            id = gets.chomp.to_i
            actual_list = self.board_list.select { |item| item.number == id } [0]
        when 5
            puts "\nBack"
            actual_board = nil
        end
        return [actual_list,actual_board]
    end

    private 
        # Call the get_data_task method from a module GetData
        # params: a list of keys to obtain user info
        def get_data(params)
            GetData.get_data_task(params)
        end
end