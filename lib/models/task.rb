# Simple Task model that represent any todo task
class Task

    @@task_number = 0 # Store the number of existing tasks in the application.
    attr_accessor :name,:description,:date,:done
    attr_reader :number

    # Constructor of class Task
    # data: represent a hash that contain the items to be assigned to the object
    def initialize(data)
        default = get_default(data)
        self.name = default[:name]
        self.description = default[:description]
        self.date = Time.now
        self.done = default[:done]
        @number = @@task_number # Work as a task id
        @@task_number += 1
    end
 
    # Update the object attributes states.
    # data: represent a hash that contain the items to be assigned to the object
    def update(data)
        default = get_default(data)
        self.name = default[:name]
        self.description = default[:description]
        self.done = default[:done]
    end
 
    # Method that describes the object as a string
    # Works as a serializator for the export to a file
    def to_s
        "#{@number} | #{self.name} | #{self.description} | #{self.date} | #{self.done}"
    end

    private 
        # Set a default values for the object attributes 
        # data: represent a hash that contain the items to be assignmed to the object
        # return: data merged with the new assigned values
        def get_default(data)
            default = {:name => "Default name", :description => "Default description", :done => false}
            default.merge!(data)
            return default
        end
end