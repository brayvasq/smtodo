# Module that help with the work with files
module StoreData

    # Method that store the data of each array element into a file.
    # data: list with elements to store.
    # path: path of the file to store the info.
    def write(data,path)
        File.open(path,"w") do |file|
            data.each {|item| file.puts item}
        end
    end
    
    # Method that read info from a file and save into an array
    # path: path of the file to read the info.
    def read(path)
        data = []
        File.open(path).each do |line|
            p line
            task = line.split("|")
            data << task
        end
        return data
    end
end