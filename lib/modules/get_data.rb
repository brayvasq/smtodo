# Module to retrieve general data
module GetData

    # Method that retireve info from the promt using a lambda function
    # array : a list of keys to obtain user info
    def self.get_data(array)
        data = lambda { |array|
            myhash = {}
            array.each_with_index do |item,index|  
                print "#{item} : "
                myhash[item] = gets.chomp
            end
            return myhash
        }
        return data.call(array)  
    end

    # Method that wrap the get_data from task info
    def self.get_data_task(params)
        return self.get_data(params)
    end
end