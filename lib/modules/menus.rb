# Module tha help with app menus
module Menus

    # Principal Method that show a menu according to a list
    # array: list of items to show into the menu
    def self.menu(array)
        puts
        puts "Welcome to Task Menu"
        menu = lambda { |array|
            array.each_with_index {|item,index| puts "#{index+1}) #{item}" unless item=="Exit"}
            puts "99) Exit" if array.include?("Exit")
            resp = gets.chomp
            return resp
        }
        return menu.call(array)        
    end

    # Wrap of menu method to show menu tasks
    def self.menu_task
        return self.menu(["Create Task","Update Task","Delete Tasks","Search Task","Show Tasks","Export","Import","Back"])
    end

    # Wrap of menu method to show menu task lists
    def self.menu_list
        return self.menu(["Create a List","Delete a List","Show Lists","Select List","Back"])
    end

    # Wrap of menu method to show menu boards
    def self.menu_board
        return self.menu(["Create a Board","Delete Board","Select Board","Show Boards","Exit"])
    end
end