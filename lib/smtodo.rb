require "smtodo/version"
require_relative './modules/menus.rb'
require_relative './models/task.rb'
require_relative './models/tasks_list.rb'
require_relative './models/board.rb'
require_relative './modules/get_data.rb'

#
# Main module of the gem
#
module Smtodo
  class Error < StandardError; end
  
  # The run method that create a loop showing the menus of TodoApp
  def self.run
    puts "SMTODO"
    val = 0 # Store the actual option choosed by the user
    boards = [] # List that contains all the boards of the app
    actual_board = nil # Contain a reference to the board choosed by the user
    actual_list = nil # Contain a reference to the list choosed by the user

    until val == 99
        # Board Menu
        unless actual_board
            val = Menus.menu_board.to_i
            case val
            when 1
                puts "\nCreate Board"
                data = GetData.get_data_task([:name])
                boards << Board.new(data[:name])
            when 2
                puts "\nDelete Boards"
                id = gets.chomp.to_i
                boards.reject! {|item| item.number == id}
            when 3
                puts "\nSelect Board"
                id = gets.chomp.to_i
                actual_board = boards.select {|item| item.number == id} [0]
            when 4
                puts "\nShow boards"
                boards.each {|item| puts "#{item.number} | #{item.name}"}
            end
        end
        
        # Alternate menus
        if actual_board
            unless actual_list
                val = Menus.menu_list.to_i
                actual_list,actual_board = actual_board.check_val(val)
            else
                val = Menus.menu_task.to_i
                actual_list = actual_list.check_action(val)? actual_list : nil
            end
        end
    end
  end
end

Smtodo.run